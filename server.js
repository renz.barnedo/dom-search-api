const express = require('express')
const axios = require('axios')
const cors = require('cors')
const morgan = require('morgan')
const compression = require('compression')

require('dotenv').config()
const app = express()
app.use(cors())
app.use(compression({ level: 9 }))
app.use(express.json())

let API_URL = process.env.API_URL_PROD
const NODE_ENV = process.env.NODE_ENV
if (NODE_ENV === 'dev') {
  API_URL = process.env.API_URL_DEV
  app.use(morgan('dev'))
}

app.post('/check', async (req, res) => {
  const fullApiUrl = API_URL + '/v1/domains/available'
  const { domains } = req.body
  try {
    const response = await axios.post(fullApiUrl, domains, {
      headers: {
        Authorization: `sso-key ${process.env.API_KEY}:${process.env.SECRET}`
      }
    })
    const { data, status, statusText } = response
    res.json({ data, status, statusText })
  } catch (error) {
    // TODO: log in a folder/txt file all errors
    console.log(error)
  }
})

PORT = process.env.PORT || 3000
app.listen(PORT, () => console.log(`Mode ${NODE_ENV} @ port ${PORT}...`))